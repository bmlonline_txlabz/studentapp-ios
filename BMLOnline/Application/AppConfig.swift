//
//  AppConfig.swift
//  Timar
//
//  Created by Afroz on 09/04/2020.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit

// MARK:- App Environment

enum AppMode: Int {
    
    case local = 0, dev, staging, production
}


var appMode: AppMode {
    get {
        return .dev
    }
}


// MARK:- App Configurations

let appConfigs = AppConfigs.sharedInstance

class AppConfigs {


    
    // MARK:- Static Properties
    
    static let sharedInstance = AppConfigs()

    
    // MARK:- Class Properties
    
    fileprivate var baseURLDomain: String {
        get {
            switch appMode {
                
            case .local:
                return "timar.herokuapp.com"

            case .dev:
                return "timar.herokuapp.com"

            case .staging:
                return "timar.herokuapp.com"
                
            case .production:
                return "timar.herokuapp.com"

            }
        }
    }

    var baseURL: String {
        get {
            if appMode == .local {
                return "http://\(baseURLDomain)/"
            }
            
            return "https://\(baseURLDomain)/"
        }
    }
}
