
//
//  AZGradientView.swift
//  DubaiHealth
//
//  Created by Afroz Zaheer on 13/07/2018.
//  Copyright © 2018 NextBridge. All rights reserved.
//

import UIKit

@IBDesignable
class AZGradientView: UIButton {
    let gradientLayer = CAGradientLayer()
    @IBInspectable var startColor:UIColor = UIColor.white{
        didSet{
            configure()
        }
    }
    
    @IBInspectable var midColor:UIColor = UIColor.init(red: 0.303, green: 0.655, blue: 0.393, alpha: 1.0) { // we only need two colors
        didSet{
            configure()
        }
    }
    
    @IBInspectable var endColor:UIColor = UIColor.white/*AppTheme.secondryColor!*/ {
        didSet{
            configure()
        }
    }
    
    @IBInspectable var direction: UInt = 1 {
        didSet{
            configure()
        }
    }
    @IBInspectable var setGradientBorderColor: Bool = false {
        didSet {
            if setGradientBorderColor {
                let gradient = CAGradientLayer()
                gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
                gradient.colors = [startColor.cgColor,midColor.cgColor,endColor.cgColor]
                gradient.cornerRadius =  self.frame.size.height / 2
                gradient.startPoint = CGPoint(x: 0, y:0 )
                gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
                
                let shape = CAShapeLayer()
                shape.lineWidth = 2
                shape.path = UIBezierPath(rect: self.bounds).cgPath
                shape.strokeColor = UIColor.black.cgColor
                shape.fillColor = UIColor.clear.cgColor
                gradient.mask = shape
                
                self.layer.addSublayer(gradient)
            }
        }
    }
    func setup() {
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func configure(){
        gradientLayer.cornerRadius = cornerRadius
        gradientLayer.frame = bounds
        gradientLayer.colors = [startColor.cgColor,midColor.cgColor,endColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y:0 )
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.locations = [0.3,0.8,0.95]

        switch direction % 5 {
        case 0:
            gradientLayer.startPoint = CGPoint(x: 0, y:0 )
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        case 1:
            gradientLayer.startPoint = CGPoint(x: 0, y:0 )
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        case 2:
            gradientLayer.startPoint = CGPoint(x: 0, y:0 )
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        case 3:
            gradientLayer.startPoint = CGPoint(x: 1, y:0 )
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            
        default:
            gradientLayer.startPoint = CGPoint(x: 1, y:0 )
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
        configure()
    }
}

