//
//  ActivityLoader.swift
//  Timar
//
//  Created by ALI on 4/11/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ActivityLoader {
    
    static let shared = ActivityLoader()
    
    let indicatorView = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: UIColor(named: "App Pink"), padding: 0)
    
    func addActivityIndicator() {
        let topControllerView = UIApplication.shared.topMostViewController()
        if let topView = topControllerView?.view
        {
            indicatorView.translatesAutoresizingMaskIntoConstraints = false
            
            topView.addSubview(indicatorView)
            topView.bringSubviewToFront(indicatorView)
            NSLayoutConstraint.activate([
                indicatorView.widthAnchor.constraint(equalToConstant: 40),
                indicatorView.heightAnchor.constraint(equalToConstant: 40),
                indicatorView.centerXAnchor.constraint(equalTo: topView.centerXAnchor),
                indicatorView.centerYAnchor.constraint(equalTo: topView.centerYAnchor)
            ])
            
            if !indicatorView.isAnimating{
                indicatorView.startAnimating()
                UIApplication.shared.beginIgnoringInteractionEvents()
            }
            
        }
    }
    func removeActivityIndicator()
    {
        if indicatorView.isAnimating{
            indicatorView.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
}
