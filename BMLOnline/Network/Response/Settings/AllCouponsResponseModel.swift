//
//  AllCouponsResponseModel.swift
//  Timar Partner
//
//  Created by Ali on 10/07/2020.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class AllCouponsResponseModel: Mappable
{
    enum CodingKeys: String {
        case id
        case code
        case counter
        case validFrom
        case validTo
        case amount
        case type
        case isActive
        case createdAt
        case updatedAt
        case CouponSalons
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var code: String?
    var counter: Int?
    var validFrom: String?
    var validTo: String?
    var amount: String?
    var type: Int?
    var isActive: Bool?
    var createdAt: String?
    var updatedAt: String?
    var couponSalons: [CouponSalonsResponseModel]?
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        code <- map[CodingKeys.code.rawValue]
        counter <- map[CodingKeys.counter.rawValue]
        validFrom <- map[CodingKeys.validFrom.rawValue]
        validTo <- map[CodingKeys.validTo.rawValue]
        amount <- map[CodingKeys.amount.rawValue]
        type <- map[CodingKeys.type.rawValue]
        isActive <- map[CodingKeys.isActive.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        couponSalons <- map[CodingKeys.CouponSalons.rawValue]
    }

}

class CouponSalonsResponseModel: Mappable
{
    enum CodingKeys: String {
        case id
        case createdAt
        case updatedAt
        case CouponId
        case SalonId
        case PackageId
        case SalonServiceId
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var createdAt: String?
    var updatedAt: String?
    var couponId: String?
    var salonId: Int?
    var packageId: String?
    var salonServiceId: String?
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        couponId <- map[CodingKeys.CouponId.rawValue]
        salonId <- map[CodingKeys.SalonId.rawValue]
        packageId <- map[CodingKeys.PackageId.rawValue]
        salonServiceId <- map[CodingKeys.SalonServiceId.rawValue]
    }

}
