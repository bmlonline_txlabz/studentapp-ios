//
//  SaloonEmployeResponseModel.swift
//  Timar Partner
//
//  Created by Ali on 10/07/2020.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class SaloonEmployeResponseModel: Mappable
{
    enum CodingKeys: String {
        case id
        case firstName
        case lastName
        case email
        case profilePictureUrl
        case shiftstart
        case shiftend
        case breakstart
        case breakend
        case isVerified
        case isBlocked
        case isActive
        case createdAt
        case updatedAt
        case SalonId
        case UserId
        case DayId
        case SalonEmployeeSkills
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var firstName: String?
    var lastName: String?
    var email: String?
    var profilePictureUrl: String?
    var shiftstart: String?
    var shiftend: String?
    var breakstart: String?
    var breakend: String?
    var isVerified: Bool?
    var isBlocked: Bool?
    var isActive: Bool?
    var createdAt: String?
    var updatedAt: String?
    var salonId: Int?
    var userId: Int?
    var dayId: Int?
    var salonEmployeeSkills: [SaloonEmployeSkills]?
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        firstName <- map[CodingKeys.firstName.rawValue]
        lastName <- map[CodingKeys.lastName.rawValue]
        email <- map[CodingKeys.email.rawValue]
        profilePictureUrl <- map[CodingKeys.profilePictureUrl.rawValue]
        shiftstart <- map[CodingKeys.shiftstart.rawValue]
        shiftend <- map[CodingKeys.shiftend.rawValue]
        breakstart <- map[CodingKeys.breakstart.rawValue]
        breakend <- map[CodingKeys.breakend.rawValue]
        isVerified <- map[CodingKeys.isVerified.rawValue]
        isBlocked <- map[CodingKeys.isBlocked.rawValue]
        isActive <- map[CodingKeys.isActive.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        salonId <- map[CodingKeys.SalonId.rawValue]
        userId <- map[CodingKeys.UserId.rawValue]
        dayId <- map[CodingKeys.DayId.rawValue]
        salonEmployeeSkills <- map[CodingKeys.SalonEmployeeSkills.rawValue]
    }
}

class SaloonEmployeSkills: Mappable
{
    enum CodingKeys: String {
        case id
        case skill
        case createdAt
        case updatedAt
        case SalonEmployeeId
        
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var skill: String?
    var createdAt: String?
    var updatedAt: String?
    var salonEmployeeId: Int?
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        skill <- map[CodingKeys.skill.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        salonEmployeeId <- map[CodingKeys.SalonEmployeeId.rawValue]
    }
}
