//
//  AddCouponResponseModel.swift
//  Timar Partner
//
//  Created by Ali on 10/07/2020.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class AddCouponResponseModel: Mappable
{
    enum CodingKeys: String {
        case isActive
        case id
        case code
        case counter
        case validFrom
        case validTo
        case amount
        case type
        case createdAt
        case updatedAt
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var code: String?
    var counter: Int?
    var validFrom: String?
    var validTo: String?
    var amount: String?
    var type: Int?
    var isActive: Bool?
    var createdAt: String?
    var updatedAt: String?
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        code <- map[CodingKeys.code.rawValue]
        counter <- map[CodingKeys.counter.rawValue]
        validFrom <- map[CodingKeys.validFrom.rawValue]
        validTo <- map[CodingKeys.validTo.rawValue]
        amount <- map[CodingKeys.amount.rawValue]
        type <- map[CodingKeys.type.rawValue]
        isActive <- map[CodingKeys.isActive.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
    }
    
}

