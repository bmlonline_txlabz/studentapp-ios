//
//  SaloonSettingsResponseModel.swift
//  Timar Partner
//
//  Created by Ali on 16/07/2020.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class SaloonSettingsResponseModel: Mappable
{
    enum CodingKeys: String {
        
        case id
        case bookingHours
        case Months
        case allowStaff
        case phone
        case email
        case createdAt
        case updatedAt
        case SalonId
        case Salon
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var bookingHours: Int?
    var months: Int?
    var allowStaff: Bool?
    var phone: Bool?
    var email: Bool?
    var createdAt: String?
    var updatedAt: String?
    var salonId: Int?
    var salon: SaloonResponseModel?
    
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        bookingHours <- map[CodingKeys.bookingHours.rawValue]
        months <- map[CodingKeys.Months.rawValue]
        allowStaff <- map[CodingKeys.allowStaff.rawValue]
        phone <- map[CodingKeys.phone.rawValue]
        email <- map[CodingKeys.email.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        salonId <- map[CodingKeys.SalonId.rawValue]
        salon <- map[CodingKeys.Salon.rawValue]
    }

}
