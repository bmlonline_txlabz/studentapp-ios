//
//  NotificationsResponseModel.swift
//  Timar Partner
//
//  Created by ALI on 5/23/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationsResponseModel: Mappable
{
    enum CodingKeys: String {
        case id
        case title
        case body
        case type
        case typeId
        case createdAt
        case updatedAt
    }

    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables

    var id: Int?
    var title: String?
    var body: String?
    var type: String?
    var typeId: Int?
    var createdAt: String?
    var updatedAt: String?
    

    // MARK: - Model mapping

    public func mapping(map: Map) {

        id <- map[CodingKeys.id.rawValue]
        title <- map[CodingKeys.title.rawValue]
        body <- map[CodingKeys.body.rawValue]
        type <- map[CodingKeys.type.rawValue]
        typeId <- map[CodingKeys.typeId.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
    }

}
