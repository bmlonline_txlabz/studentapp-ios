//
//  BookingsResponseModel.swift
//  Timar Partner
//
//  Created by ALI on 5/23/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class BookingsResponseModel: Mappable
{
    enum CodingKeys: String {
        case id
        case bookingDate
        case startTime
        case endTime
        case price
        case discountAmount
        case name
        case phone
        case email
        case stauts
        case createdAt
        case updatedAt
        case UserId
        case SalonEmployeeId
        case CouponId
        case SalonId
        case Salon
        case User
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var bookingDate: String?
    var startTime: String?
    var endTime: String?
    var price: String?
    var discountAmount: String?
    var name: String?
    var phone: String?
    var email: String?
    var stauts: Bool?
    var createdAt: String?
    var updatedAt: String?
    var userId: Int?
    var salonEmployeeId: Int?
    var couponId: Int?
    var salonId: Int?
    var salon: SaloonResponseModel?
    var user: User?
    
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        bookingDate <- map[CodingKeys.bookingDate.rawValue]
        startTime <- map[CodingKeys.startTime.rawValue]
        endTime <- map[CodingKeys.endTime.rawValue]
        price <- map[CodingKeys.price.rawValue]
        discountAmount <- map[CodingKeys.discountAmount.rawValue]
        name <- map[CodingKeys.name.rawValue]
        phone <- map[CodingKeys.phone.rawValue]
        email <- map[CodingKeys.email.rawValue]
        stauts <- map[CodingKeys.stauts.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        userId <- map[CodingKeys.UserId.rawValue]
        salonEmployeeId <- map[CodingKeys.SalonEmployeeId.rawValue]
        couponId <- map[CodingKeys.CouponId.rawValue]
        salonId <- map[CodingKeys.SalonId.rawValue]
        salon <- map[CodingKeys.Salon.rawValue]
        user <- map[CodingKeys.User.rawValue]
    }

}
