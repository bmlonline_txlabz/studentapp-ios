//
//  SaloonResponseModel.swift
//  Timar Partner
//
//  Created by ALI on 5/23/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class SaloonResponseModel: Mappable {
    enum CodingKeys: String {
        case id
        case name
        case description
        case SalonType
        case isFeatured
        case plotNum
        case street
        case area
        case lat
        case lon
        case isActive
        case createdAt
        case updatedAt
        case CityId
        case OrganizationId
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var id: Int?
    var name: String?
    var description: String?
    var salonType: String?
    var isFeatured: Bool?
    var plotNum: String?
    var street: String?
    var area: String?
    var lat: Double?
    var lon: Double?
    var isActive: Bool?
    var createdAt: String?
    var updatedAt: String?
    var cityId: Int?
    var organizationId: Int?
    
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        id <- map[CodingKeys.id.rawValue]
        name <- map[CodingKeys.name.rawValue]
        description <- map[CodingKeys.description.rawValue]
        salonType <- map[CodingKeys.SalonType.rawValue]
        isFeatured <- map[CodingKeys.isFeatured.rawValue]
        plotNum <- map[CodingKeys.plotNum.rawValue]
        street <- map[CodingKeys.street.rawValue]
        area <- map[CodingKeys.area.rawValue]
        lat <- map[CodingKeys.lat.rawValue]
        lon <- map[CodingKeys.lon.rawValue]
        isActive <- map[CodingKeys.isActive.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        cityId <- map[CodingKeys.CityId.rawValue]
        organizationId <- map[CodingKeys.OrganizationId.rawValue]
    }

}
