//
//  SignInResponseModel.swift
//  Timar Partner
//
//  Created by ALI on 4/14/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    // MARK: - Model Keys
    enum CodingKeys: String {
        case id
        case name
        case email
        case password
        case imageUrl
        case isActive
        case createdAt
        case updatedAt
        case SalonId
    }

    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables

    var id: Int?
    var name: String?
    var email: String?
    var password: String?
    var imageUrl: String?
    var isActive: Bool?
    var createdAt: String?
    var updatedAt: String?
    var salonId: Int?
    

    // MARK: - Model mapping

    public func mapping(map: Map) {

        id <- map[CodingKeys.id.rawValue]
        name <- map[CodingKeys.name.rawValue]
        email <- map[CodingKeys.email.rawValue]
        password <- map[CodingKeys.password.rawValue]
        imageUrl <- map[CodingKeys.imageUrl.rawValue]
        isActive <- map[CodingKeys.isActive.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
        updatedAt <- map[CodingKeys.updatedAt.rawValue]
        salonId <- map[CodingKeys.SalonId.rawValue]
    }
}
