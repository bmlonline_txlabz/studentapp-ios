//
//  AppFeedBackResponseModel.swift
//  Timar
//
//  Created by ALI on 4/13/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class AppFeedBackResponseModel: Mappable {
    enum CodingKeys: String {
        
        case id
        case feedback
        case createdAt
    }

    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables

    var id: Int?
    var feedback: String?
    var createdAt: String?
    // MARK: - Model mapping

    public func mapping(map: Map) {

        id <- map[CodingKeys.id.rawValue]
        feedback <- map[CodingKeys.feedback.rawValue]
        createdAt <- map[CodingKeys.createdAt.rawValue]
    }
}
