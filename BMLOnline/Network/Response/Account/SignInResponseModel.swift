//
//  SignInResponseModel.swift
//  Timar Partner
//
//  Created by ALI on 4/14/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import Foundation
import ObjectMapper

class SignInResponseModel: Mappable {
    // MARK: - Model Keys
    enum CodingKeys: String {
        case user
        case token
        
    }

    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables

    var user: User?
    var token: String?
    

    // MARK: - Model mapping

    public func mapping(map: Map) {

        user <- map[CodingKeys.user.rawValue]
        token <- map[CodingKeys.token.rawValue]
    }
}
