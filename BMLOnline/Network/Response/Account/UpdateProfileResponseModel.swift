//
//  UpdateProfileResponseModel.swift
//  Timar Partner
//
//  Created by ALI on 5/7/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import UIKit
import ObjectMapper

class UpdateProfileResponseModel: Mappable {
    enum CodingKeys: String {
        case user        
    }
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
           var user: User?
    
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        user <- map[CodingKeys.user.rawValue]
    }
}
