//
//  BaseResponseModel.swift
//  Timar
//
//  Created by ALI on 4/9/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseResponseModel<T: Mappable>: Mappable {
    // MARK: - Model Keys
    
    enum CodingKeys: String {
        case code
        case status
        case data
        case message

    }
    
    // MARK: - Inilizers
    
    public required init?(map: Map) {
        mapping(map: map)
    }
    // MARK: - Model Variables
    
    var code: Int = 1000
    var status: Bool?
    var data: T?
    var dataArray: [T]?
    var message: String?
    
    // MARK: - Model mapping
    
    public func mapping(map: Map) {
        
        code <- map[CodingKeys.code.rawValue]
        status <- map[CodingKeys.status.rawValue]
        if map[CodingKeys.data.rawValue].currentValue is Array<[String: Any]>{
            dataArray <- map[CodingKeys.data.rawValue]
        }
        else{
            data <- map[CodingKeys.data.rawValue]
        }
        
        message <- map[CodingKeys.message.rawValue]

    }
}
