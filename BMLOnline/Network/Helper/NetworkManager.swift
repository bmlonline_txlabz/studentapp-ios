//
//  MyNetworkManager.swift
//  FANetworkLayer
//
//  Created by fahid.attique on 10/01/2020.
//  Copyright © 2020 fahid.attique. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

let networkManager = NetworkManager.shared

class NetworkManager: APIRoutable {

    var sessionManager: APISessionManager = APISessionManager()
    static let shared = NetworkManager()
    private init() {}
    
    func getHeaders(_ authorized: Bool, _ additionalHeaders: [String : String]?) -> [String : String]? {
        
        ActivityLoader.shared.addActivityIndicator()
        
        var headers = [String: String]()
//        headers["Content-Type"] = "application/json"
//        headers[""] = ""
        if authorized {
            if let addtionalHeaderFields = additionalHeaders {
                addtionalHeaderFields.forEach { (key, value) in
                    headers[key] = value
                }
            }
        }
        
        
        return headers
    }
    
    func validate(dataResponse: DataResponse<Any>, with completion: @escaping API.Completion<Any?>.simple) {
        
        ActivityLoader.shared.removeActivityIndicator()
        
        if let error = dataResponse.error {
            let errorMessage = errorMessageFromAPIError(error: error)
            if !errorMessage.isEmpty {
                completion(.failure(NSError(errorMessage: errorMessage, code: dataResponse.response?.statusCode)))
                return
            }
        }
        guard let value = dataResponse.value as? [String:Any] else {
            let message = API.shouldShowDevLogs ? "Response Value from server is nil." : APIErrorMessage.internalServerError
            return completion(.failure(NSError(errorMessage: message, code: APIErrorCodes.responseNil)))
        }
        guard let code = value["code"] as? Int,  let message = value["message"] as? String else {
            let message = API.shouldShowDevLogs ? "Response Value from server is nil." : APIErrorMessage.internalServerError
            return completion(.failure(NSError(errorMessage: message, code: APIErrorCodes.responseNil)))
        }
        
        if 200 ... 299 ~= code {
            
            completion(.success(value))
        } else {
            completion(.failure(NSError(errorMessage: message, code: code)))
        }
    }
    
}
