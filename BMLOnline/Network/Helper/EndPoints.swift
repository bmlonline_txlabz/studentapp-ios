//
//  EndPoints.swift
//  FANetworkLayer
//
//  Created by fahid.attique on 10/01/2020.
//  Copyright © 2020 fahid.attique. All rights reserved.
//

import Foundation

enum TimarApiEndPoints: URLDirectable {
    
    case signIn
    case updateProfile(Int = 0)
    case updatePassword(Int = 0)
    case bookings(Int = 0)
    case notification(Int = 0)
    case appfeedback
    case bookingStatusChange(Int = 0)
    case rejectBooking(Int = 0)
    case allCoupons
    case saloonEmployes
    case saloonSettings(Int = 0)
    func urlString() -> String {
     
        var endpoint = ""
        
        switch (self) {
                        
        case .signIn:
            endpoint = "salons/manager/signIn"
        case .updateProfile(let id):
            endpoint = "salons/manager/editProfile/\(id)"
        case .updatePassword(let id):
            endpoint = "salons/manager/changePassword/\(id)"
        case .bookings(let id):
            endpoint = "bookings/manager/\(id)"
        case .notification(let id):
            endpoint = "notification/manager/\(id)"
        case .appfeedback:
            endpoint = "appfeedback"
        case .rejectBooking(let id):
            endpoint = "bookings/reject/\(id)"
        case .bookingStatusChange(let id):
            endpoint = "bookings/status/\(id)"
        case .allCoupons:
            endpoint = "coupons"
        case .saloonEmployes:
            endpoint = "employees"
        case .saloonSettings(let saloonId):
            endpoint = "salons/settings/\(saloonId)"
        }
        
        return AppConfigs.sharedInstance.baseURL + endpoint
    }
}
