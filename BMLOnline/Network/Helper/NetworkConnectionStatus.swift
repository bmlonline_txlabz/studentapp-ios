//
//  NetworkConnectionStatus.swift
//  Lawon
//
//  Created by ALI on 2/18/20.
//  Copyright © 2020 mindslab. All rights reserved.
//

import Foundation
import Alamofire


public enum NetworkConnectionStatus: String {
    case online
    case offline
}

public protocol NetworkConnectionStatusListner: class {
    func networkStatusDidChange(status: NetworkConnectionStatus)
}

class ConectivityManager: NSObject {
    

    private override init() {
        super.init()
        configureReachability()
    }
    private static let _shared = ConectivityManager()
    
    class func shared() -> ConectivityManager{
        return _shared
    }
    
    private let networkReachability: NetworkReachabilityManager? = NetworkReachabilityManager()
    
    var isNetworkAvailable: Bool {
        return networkReachability?.isReachable ?? false
    }
    
    var listners = [NetworkConnectionStatusListner]()
    
    private func configureReachability(){
        networkReachability?.listener = { [weak self] status in
            switch status {
            case .unknown:
                self?.notifyAllListnersWith(status: .offline)

            case .notReachable:
                self?.notifyAllListnersWith(status: .offline)
            case .reachable(.ethernetOrWiFi):
                fallthrough
            case .reachable(.wwan):
                self?.notifyAllListnersWith(status: .online)
            }
        }
    }
    
    private func notifyAllListnersWith(status: NetworkConnectionStatus){
        for listner in listners{
            listner.networkStatusDidChange(status: status)
        }
    }
    
    func addListner(listner: NetworkConnectionStatusListner){
        listners.append(listner)
    }
    
    func removeListner(listner: NetworkConnectionStatusListner){
        listners = listners.filter {
            $0 !== listner
        }
    }
    
    func startListning(){
        networkReachability?.startListening()
    }
    
    func stopListning(){
        networkReachability?.stopListening()
    }
}
